angular.module('app.controllers', [])

.controller('loginCtrl', ['$scope', '$stateParams','$state', '$cordovaSQLite',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $cordovaSQLite) {

    $scope.user = {
          login: 'user por defecto',
          password: ''
      };

      $scope.error="";

  //Función de navegación
    $scope.login=function(){
      console.log($scope.user.user);
      if($scope.user.user === "admin" && $scope.user.password=="admin" ){
        $state.go("home");
      }else{
        $scope.error="datos incorrectos";
      }

    };

    $scope.register=function(){
      $state.go("register");
    };


    $scope.login1=function(){
      console.log("aquí vamos a consultar...");
      if (window.cordova) {
            db = $cordovaSQLite.openDB({ name: "my.db",iosDatabaseLocation:'default' }); //device



          }else{
            db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
          }
      db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM Demo WHERE login = ?', [$scope.user.login], function(tx, rs) {
          for(i=0;i<rs.rows.length;i++){
            if($scope.user.login === rs.rows.item(i).login && $scope.user.password==rs.rows.item(i).password ){
              $state.go("home");
            }
          }
          $scope.error="datos incorrectos";
        }, function(tx, error) {
          console.log('SELECT error: ' + error.message);
          $scope.resultado.data=error.message;
        });
      });
    };

}])

.controller('homeCtrl', ['$scope', '$stateParams','PostService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,PostService) {
  $scope.response={
      data:''
  }
  $scope.consumir=function(){

      PostService.consumir().then(function(response){
        $scope.response.data = response.data
        console.log($scope.response.data);
      });

  };

}])

.controller('registerCtrl', ['$scope', '$stateParams', '$cordovaSQLite', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $cordovaSQLite) {

  $scope.user = {
        login: '',
        password: '',
        email: ''
    };

  $scope.resultado = {
        data : ''
    };

  $scope.register=function(){
    console.log("aquí vamos a guardar...");
    if (window.cordova) {
          db = $cordovaSQLite.openDB({ name: "my.db",iosDatabaseLocation:'default'  }); //dispositivo usa sqlite
        }else{
          db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser usa websql
        }
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS Demo (login, password,email)');
        tx.executeSql('INSERT INTO Demo VALUES (?,?,?)', [$scope.user.login, $scope.user.password,$scope.user.email]);
      }, function(error) {
        console.log('Transaction ERROR: ' + error.message);
      }, function() {
        console.log('Populated database OK');
      });
  };

  $scope.consultar=function(){
    console.log("aquí vamos a consultar...");
    if (window.cordova) {
          db = $cordovaSQLite.openDB({ name: "my.db",iosDatabaseLocation:'default'  }); //device
        }else{
          db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
        }
    db.transaction(function(tx) {
      tx.executeSql('SELECT * FROM Demo WHERE login = ?', [$scope.user.login], function(tx, rs) {
        for(i=0;i<rs.rows.length;i++){
          console.log('Data: ' + rs.rows.item(i).login+ " "+rs.rows.item(i).password+" "+rs.rows.item(i).email);
          resultado= rs.rows.item(i).login+ " "+rs.rows.item(i).password+" "+rs.rows.item(i).email;
          $scope.resultado.data= resultado;
        }

      }, function(tx, error) {
        console.log('SELECT error: ' + error.message);
        $scope.resultado.data=error.message;
      });
    });
  };


}])
